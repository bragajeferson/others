from math import floor, log, pow

unity = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
size = len("""<conteudo>""")
power = floor(log(size, 1024)) if size > 0 else 0
filelength = str(round(size / pow(1024, power), 2)) + ' ' + unity[power]
print(filelength)